﻿using System.Collections;
using System.Net.Http.Headers;
using System.Xml.Serialization;

namespace ContactManager
{

    /* Classe de base qui servir à construire un Dossier et un Contact
     */
    public abstract class Fichier
    {
        [XmlIgnore]
        public Dossier? Parent; // Parent vaut null si c'est le dossier root
        public string Nom { get; set; } = "nom?";
       
        public string DateCreation { get; set; } = GetDateActuelle();

        public string DateModification { get; set; } = GetDateActuelle();

        public override string ToString()
        {
            return String.Format(
                "Fichier : {0} (creation : {1}, derniere modif : {2})",
                Nom, DateCreation, DateModification);
        }

        /* Renvoie la date actuelle sous forme d'une chaîne de caractère */
        public static string GetDateActuelle()
        {
            return DateTime.Now.ToString("dd/MM/yyyy HH:mm");
        }
            
        /* Enlève le fichier du dossier dans lequle il se situe */
        public void Supprimer()
        {
            Parent?.SupprimerFichier(this);
        }

        /* Renvoie si un fichier est un dossier */
        public bool EstUnDossier()
        {
            return GetType() == typeof(Dossier);
        }
    }

    /* Classe qui représente un dossier, c'est à dire une entité capable de 
     * stocker des Fichiers
     */
    public class Dossier : Fichier//, IEnumerable
    {   
        // Le membre aurait dû être privé mais on est obligé de casser l'encapsulation 
        // pour que ça puisse être sérialisé par XMLSerializer
        public List<Fichier> fichiers = [];

        public IEnumerator GetEnumerator()
        {
            return fichiers.GetEnumerator();
        }
        
        /* Ajoute un fichier au dossier */
        public void ajouterFichier(Fichier fichier)
        {
            fichier.Parent = this;
            fichiers.Add(fichier);
        }

        /* Supprime un fichier du dossier */
        public void SupprimerFichier(Fichier fichier)
        {
            if (fichier.Parent != this) 
            {
                throw new Exception("Le fichier n'est pas dans le dossier.");
            }

            fichiers.Remove(fichier);
        }

        public override string ToString()
        {
            return String.Format(
                "Dossier : {0} (creation : {1}, derniere modif : {2})",
                Nom, DateCreation, DateModification);
        }

        public void AfficherArboresence(int level)
        {
            foreach (Fichier fichier in fichiers)
            {
                for (int k = 0; k < level; k++)
                {
                    Console.Write(" ");
                }
                Console.Write("| ");

                Console.WriteLine(fichier.ToString());

                if (fichier.EstUnDossier())
                {
                    ((Dossier)fichier).AfficherArboresence(level + 1);
                }
            }
        }

        /* Comme l'attribut Parent d'un Fichier ne peut pas être stocké dans un 
         * fichier, il faut le recalculer quand une arborescence est chargée
         */
        public void RedefinirParent()
        {
            foreach (Fichier fichier in fichiers)
            {
                fichier.Parent = this;
                if (fichier.EstUnDossier())
                {
                    ((Dossier)fichier).RedefinirParent();
                }
            }
        }
    }

    /* Classe qui représente un contact
     */
    public class Contact : Fichier
    {
        public string Prenom { get; set; } = "prenom?";
        public string Courriel { get; set; } = "courriel?";
        public string Societe { get; set; } = "societe?";
        public string Lien { get; set; } = "lien?";


        public override string ToString()
        {
            return String.Format(
                "Contact : {0} {1} ({2}) {3}, lien : {4}",
                Prenom, Nom, Societe, Courriel, Lien);
        }
    }
}


