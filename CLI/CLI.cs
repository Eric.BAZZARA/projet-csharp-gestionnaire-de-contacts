﻿using ContactManager;
using Serializer;
using System.Runtime.Serialization.Json;

namespace CLI
{
    internal class CLI
    {
        private static void Main(string[] args)
        {
            IntefaceTextuelle app = new();

            app.MainLoop();
        }
    }

    /* Liste des commandes disponibles */
    enum Commandes { QUITTER, AFFICHER, CHARGER, ENREGISTER, AJOUTER_DOSSIER, AJOUTER_CONTACT,
        CD_FRERE_SUIVANT, CD_FRERE_PRECEDENT, CD_PARENT, CD_ENFANT, LS};

    /* Classe qui permet à l'utilisateur d'utiliser son application de gestion de 
     * contacts en mode console
     */
    class IntefaceTextuelle
    {
        readonly string listeDesCommandes = @"
        Liste des commandes disponibles : 
        'q' : quitter l'application,
        'a' : afficher les données de l'application,
        'ls': afficher l'arborescence à partir du dossier courant

        'c' : charger un fichier,
        'e' : enregistrer les données dans un fichier,

        'd' : ajouter un dossier,
        'z' : ajouter un contact,

        'sq': aller au dossier parent,
        'sd': aller au premier dossier enfant,
        'sz': aller au dossier frère précédent,
        'sx': aller au dossier frère suivant
        ";

        SerializerService serializerService= new SerializerService();

        Dossier dossierRoot = new() { Nom = "root" };

        Dossier dossierCourant;

        public IntefaceTextuelle()
        {
            dossierCourant = dossierRoot;
        }

        /* Contient la boucle principale de l'application console */
        public void MainLoop()
        {
            Console.WriteLine("Bienvenue dans le gestionnaire de contacts en " +
                "mode ligne de commande.");
            Console.WriteLine(listeDesCommandes);
            bool continuer = true;

            while (continuer)
            {
                Commandes cmd = RecupererCommande();
                Console.WriteLine("Commande : " + cmd);

                switch (cmd)
                {
                    case Commandes.QUITTER:
                        continuer = false; break;

                    case Commandes.AJOUTER_DOSSIER:
                        AjouterDossier(); break;

                    case Commandes.AFFICHER:
                        AfficherArborescence(); break;

                    case Commandes.AJOUTER_CONTACT:
                        AjouterContact(); break;

                    case Commandes.ENREGISTER:
                        EnregistrerDonnees();break;

                    case Commandes.CHARGER:
                        RecupererDonnees();break;

                    case Commandes.CD_PARENT:
                        CD_DossierParent(); break;

                    case Commandes.CD_ENFANT:
                        CD_DossierEnfant(); break;

                    case Commandes.CD_FRERE_PRECEDENT:
                        CD_DossierFrerePrecedent();break;

                    case Commandes.CD_FRERE_SUIVANT:
                        CD_DossierFrereSuivant(); break;

                    case Commandes.LS:
                        dossierCourant.AfficherArboresence(0);break;

                    default:
                        Console.WriteLine("Commande non implementé encore");
                        break;
                }
                Console.WriteLine("");
            }
        }

        /* Renvoie la commande que l'utilisateur a envie d'exécuter */
        private Commandes RecupererCommande()
        {
            Commandes res = Commandes.AFFICHER;
            bool cmd_ok = false;

            while (!cmd_ok)
            {
                Console.Write($"Que voulez-vous faire ? [{dossierCourant.Nom}] >>> ");
                string cmd_str = Console.ReadLine() ?? "";
                cmd_str = cmd_str.ToLower();

                cmd_ok = true;
                switch (cmd_str)
                {
                    case "q":
                        res = Commandes.QUITTER; break;
                    case "a":
                        res = Commandes.AFFICHER; break;
                    case "c":
                        res = Commandes.CHARGER; break;
                    case "e":
                        res = Commandes.ENREGISTER; break;
                    case "d":
                        res = Commandes.AJOUTER_DOSSIER; break;
                    case "z":
                        res = Commandes.AJOUTER_CONTACT; break;

                    case "sq":
                        res = Commandes.CD_PARENT; break;
                    case "sd":
                         res = Commandes.CD_ENFANT; break;
                    case "sz":
                        res = Commandes.CD_FRERE_PRECEDENT; break;
                    case "sx":
                        res = Commandes.CD_FRERE_SUIVANT; break;

                    case "ls":
                        res = Commandes.LS;break;

                    default:
                        cmd_ok = false;
                        Console.WriteLine("Commande inconnue : '{0}'", cmd_str);
                        Console.WriteLine(listeDesCommandes);
                        break;
                }
            }

            return res;
        }

        /* Permet à l'utilisateur de donner les informations nécessaires pour 
         * créer un dossier
         */
        private void AjouterDossier()
        {
            string? input_txt;

            Console.Write("Nom du dossier : ");
            input_txt = Console.ReadLine();
            if (input_txt == null || input_txt == "")
            {
                Console.WriteLine("Nom de dossier invalide abandon.");
                return;
            }

            Dossier new_dossier = new() { Nom = input_txt };

            dossierCourant.ajouterFichier(new_dossier);
            dossierCourant = new_dossier;

            Console.WriteLine("Dossier crée avec succès");
        }

        /* Permet à l'utilisateur de donner les informations nécessaires pour 
         * créer un contact
         */
        private void AjouterContact()
        {
            Contact contact = new();

            Console.Write("Prénom : ");
            contact.Prenom = Console.ReadLine() ?? "prénom?";

            Console.Write("Nom : ");
            contact.Nom = Console.ReadLine() ?? "nom?";

            Console.Write("Courriel : ");
            contact.Courriel = Console.ReadLine() ?? "courriel?";

            Console.Write("Société : ");
            contact.Societe = Console.ReadLine() ?? "société?";

            Console.Write("Lien : ");
            contact.Lien = Console.ReadLine() ?? "lien?";

            dossierCourant.ajouterFichier(contact);
            Console.WriteLine("Contact créé\n");
        }

        /* Affiche l'arborescence des fichiers
         */
        private void AfficherArborescence()
        {
            Console.WriteLine(dossierRoot.ToString());
            dossierRoot.AfficherArboresence(1);
        }

        /* Permet à l'utilisateur de saisir un mot de passe pour sauvegarder 
         * ses données
         */
        private void EnregistrerDonnees()
        {
            // Récupération du mot de passe
            Console.Write("Mot de passe : ");
            string mdp = Console.ReadLine() ?? "";
            if (mdp == "") mdp = "0000";

            // Appel de la fonction de sauvegarde
            serializerService.Serialize(dossierRoot, mdp);

            Console.WriteLine("Fichier enregistré");
        }

        /* Permet à l'utilisateur de saisir un mot de passe pour récupérer 
         * ses données
         */
        private void RecupererDonnees()
        {
            // Récupération du mot de passe
            Console.Write("Mot de passe : ");
            string mdp = Console.ReadLine() ?? "";
            if (mdp == "") mdp = "0000";

            // Appel de la fonction de lecture
            try
            {
                dossierRoot = serializerService.Deserialize(mdp);
                dossierCourant = dossierRoot;
                Console.WriteLine("Fichier chargé");
            }
            catch (InvalidOperationException)
            {   
                Console.WriteLine("Mauvais mot de passe.");
            }
        }

        /* Change la valeur du dossier courant à la valeur du dossier parent */
        private void CD_DossierParent()
        {
            if(dossierCourant.Parent != null)
            {
                dossierCourant = dossierCourant.Parent;
            }
            else
            {
                Console.WriteLine("Le dossier root n'a pas de parent.");
            }
        }

        /* Change la valeur du dossier courant au premier dossier enfant 
         * rencontré dans le dossier courant. S'il n'y pas de dossier, sa 
         * valeur reste inchangée
         */
        private void CD_DossierEnfant()
        {
            bool trouve = false;

            foreach (Fichier fichier in dossierCourant)
            {
                if (fichier.EstUnDossier())
                {
                    dossierCourant = (Dossier)fichier;
                    trouve = true;
                    break;
                }
            }

            if (!trouve)
            {
                Console.WriteLine("Il n'y a aucun dossier dans le dossier actuel.");
            }

        }

        /* Change la valeur du dossier courant au prochain dossier qui est situé
         * dans le dossier parent
         */
        private void CD_DossierFrereSuivant()
        {
            if (dossierCourant.Parent == null)
            {
                Console.WriteLine("Le dossier root n'a pas de frère.");
            }
            else
            {
                List<Fichier> fichiers = dossierCourant.Parent.fichiers;
                int index = fichiers.IndexOf(dossierCourant);
                int k;
                for (k = index + 1; k < fichiers.Count; k++)
                {
                    if (fichiers[k].EstUnDossier())
                    {
                        dossierCourant = (Dossier)fichiers[k];
                        break;
                    }
                }
                
                if (k == fichiers.Count)
                {
                    Console.WriteLine("Il n'y a pas de dossier après.");
                }
            }
        }

        /* Change la valeur du dossier courant au dossier précédent qui est situé
         * dans le dossier parent
         */
        private void CD_DossierFrerePrecedent()
        {
            if (dossierCourant.Parent == null)
            {
                Console.WriteLine("Le dossier root n'a pas de frère.");
            }
            else
            {
                List<Fichier> fichiers = dossierCourant.Parent.fichiers;
                int index = fichiers.IndexOf(dossierCourant);
                int k;
                for (k = index - 1; k >= 0; k--)
                {
                    if (fichiers[k].EstUnDossier())
                    {
                        dossierCourant = (Dossier)fichiers[k];
                        break;
                    }
                }

                if (k == -1)
                {
                    Console.WriteLine("Il n'y a pas de dossier avant.");
                }
            }
        }
    }
}