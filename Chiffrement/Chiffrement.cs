﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Chiffrement
{   
    /* Classe qui fournit des méthodes pour chiffrer et déchiffrer des données
     */
    public class CryptoService
    {
        static byte[] salt = Encoding.UTF8.GetBytes("Le sel ça donne du goût à tout ce que l'on veut.");

        /* Renvoie un stream dans lequel les données écrites sont chiffrées
         */
        static public StreamWriter GetSecureStream(string filePath, string password) 
        {
            Rfc2898DeriveBytes rfc2898DeriveBytes = new (password, salt, 1000, HashAlgorithmName.SHA512);
            FileStream fStream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write);
            CryptoStream cStream = new (
                fStream,
                Aes.Create().CreateEncryptor(
                    rfc2898DeriveBytes.GetBytes(32), 
                    rfc2898DeriveBytes.GetBytes(16)),
                CryptoStreamMode.Write);

            return  new StreamWriter(cStream);
        }

        /* Renvoie un stream dans lequel les données lues sont déchiffrées
         */
        static public StreamReader GetDecryptedStream(string filePath, string password)
        {
            Rfc2898DeriveBytes rfc2898DeriveBytes = new (password, salt, 1000, HashAlgorithmName.SHA512);
            FileStream fStream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Read);
            CryptoStream cStream = new(
                fStream,
                Aes.Create().CreateDecryptor(
                    rfc2898DeriveBytes.GetBytes(32),
                    rfc2898DeriveBytes.GetBytes(16)),
                CryptoStreamMode.Read);

            return new StreamReader(cStream);
        }


    }
    
}