﻿
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using ContactManager;
using Chiffrement;

namespace Serializer
{
    public class SerializerService
    {
        string filePath2 = "test_sauvegarde";
        string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\contact_db";

        //BinaryFormatter binaryFormatter = new BinaryFormatter();
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Dossier), new Type[] { typeof(Contact), typeof(Fichier) });

        public void Serialize(Dossier root, string mdp)
        {
            // Création d'un flux de chiffrement
            StreamWriter sw = CryptoService.GetSecureStream(filePath, mdp);

            // Ecriture des données
            xmlSerializer.Serialize(sw, root);
            //binaryFormatter.Serialize(fs, root);

            sw.Close();
        }

        public Dossier Deserialize(string mdp)
        {
            Dossier? res;

            // Création d'un flux de déchiffrement
            StreamReader sr = CryptoService.GetDecryptedStream(filePath, mdp);

            try
            {
                res = (Dossier)xmlSerializer.Deserialize(sr);
            }
            catch (InvalidOperationException)
            {
                sr.Close();
                throw;
            }

            // Si l'opération a échouée, on crée nouveau dossier root
            if (res == null)
            {
                res = new Dossier() { Nom = "root" };
            }
            else // Sinon on recalcule l'attribut Parent des fichiers
            {
                res.RedefinirParent();
            }

            sr.Close();

            return res;
        }
    }
}