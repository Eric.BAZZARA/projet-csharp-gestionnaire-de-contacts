﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ContactManager;

namespace GUI
{
    public partial class FormDossier : Form
    {
        Dossier DossierInfo { get; set; }
        public FormDossier(Dossier dossier)
        {
            InitializeComponent();

            DossierInfo = dossier;

            textBoxNom.Text = DossierInfo.Nom;
            labelVarDateCreation.Text = DossierInfo.DateCreation;
            labelVarDateModif.Text = DossierInfo.DateModification;

            // Cas particulier du dossier root
            if (DossierInfo.Parent == null) textBoxNom.Enabled = false;
        }

        private void buttonValider_Click(object sender, EventArgs e)
        {
            DossierInfo.Nom = textBoxNom.Text;
            DossierInfo.DateModification = Fichier.GetDateActuelle();

            Close();
        }

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
