﻿namespace GUI
{
    partial class FormDossier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel2 = new Panel();
            labelVarDateModif = new Label();
            labelDateModification = new Label();
            labelVarDateCreation = new Label();
            labelDateCreation = new Label();
            panel1 = new Panel();
            buttonAnnuler = new Button();
            buttonValider = new Button();
            textBoxNom = new TextBox();
            labelNom = new Label();
            panel2.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // panel2
            // 
            panel2.Controls.Add(labelVarDateModif);
            panel2.Controls.Add(labelDateModification);
            panel2.Controls.Add(labelVarDateCreation);
            panel2.Controls.Add(labelDateCreation);
            panel2.Location = new Point(305, 23);
            panel2.Name = "panel2";
            panel2.Size = new Size(223, 129);
            panel2.TabIndex = 10;
            // 
            // labelVarDateModif
            // 
            labelVarDateModif.AutoSize = true;
            labelVarDateModif.Location = new Point(3, 74);
            labelVarDateModif.Name = "labelVarDateModif";
            labelVarDateModif.Size = new Size(38, 15);
            labelVarDateModif.TabIndex = 11;
            labelVarDateModif.Text = "[date]";
            // 
            // labelDateModification
            // 
            labelDateModification.AutoSize = true;
            labelDateModification.Location = new Point(3, 59);
            labelDateModification.Name = "labelDateModification";
            labelDateModification.Size = new Size(128, 15);
            labelDateModification.TabIndex = 10;
            labelDateModification.Text = "Dernière modification :";
            // 
            // labelVarDateCreation
            // 
            labelVarDateCreation.AutoSize = true;
            labelVarDateCreation.Location = new Point(3, 25);
            labelVarDateCreation.Name = "labelVarDateCreation";
            labelVarDateCreation.Size = new Size(38, 15);
            labelVarDateCreation.TabIndex = 9;
            labelVarDateCreation.Text = "[date]";
            // 
            // labelDateCreation
            // 
            labelDateCreation.AutoSize = true;
            labelDateCreation.Location = new Point(3, 10);
            labelDateCreation.Name = "labelDateCreation";
            labelDateCreation.Size = new Size(99, 15);
            labelDateCreation.TabIndex = 8;
            labelDateCreation.Text = "Date de création :";
            // 
            // panel1
            // 
            panel1.Controls.Add(buttonAnnuler);
            panel1.Controls.Add(buttonValider);
            panel1.Location = new Point(31, 70);
            panel1.Name = "panel1";
            panel1.Size = new Size(175, 35);
            panel1.TabIndex = 11;
            // 
            // buttonAnnuler
            // 
            buttonAnnuler.Location = new Point(84, 3);
            buttonAnnuler.Name = "buttonAnnuler";
            buttonAnnuler.Size = new Size(75, 23);
            buttonAnnuler.TabIndex = 1;
            buttonAnnuler.Text = "Annuler";
            buttonAnnuler.UseVisualStyleBackColor = true;
            buttonAnnuler.Click += buttonAnnuler_Click;
            // 
            // buttonValider
            // 
            buttonValider.Location = new Point(3, 3);
            buttonValider.Name = "buttonValider";
            buttonValider.Size = new Size(75, 23);
            buttonValider.TabIndex = 0;
            buttonValider.Text = "Valider";
            buttonValider.UseVisualStyleBackColor = true;
            buttonValider.Click += buttonValider_Click;
            // 
            // textBoxNom
            // 
            textBoxNom.Location = new Point(31, 41);
            textBoxNom.Name = "textBoxNom";
            textBoxNom.Size = new Size(179, 23);
            textBoxNom.TabIndex = 13;
            textBoxNom.TextChanged += textBoxNom_TextChanged;
            // 
            // labelNom
            // 
            labelNom.AutoSize = true;
            labelNom.Location = new Point(31, 23);
            labelNom.Name = "labelNom";
            labelNom.Size = new Size(34, 15);
            labelNom.TabIndex = 12;
            labelNom.Text = "Nom";
            // 
            // FormDossier
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(577, 220);
            Controls.Add(textBoxNom);
            Controls.Add(labelNom);
            Controls.Add(panel1);
            Controls.Add(panel2);
            Name = "FormDossier";
            Text = "FormDossier";
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            panel1.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Panel panel2;
        private Label labelVarDateModif;
        private Label labelDateModification;
        private Label labelVarDateCreation;
        private Label labelDateCreation;
        private Panel panel1;
        private Button buttonAnnuler;
        private Button buttonValider;
        private TextBox textBoxNom;
        private Label labelNom;
    }
}