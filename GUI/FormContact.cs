﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ContactManager;

namespace GUI
{
    public partial class FormContact : Form
    {
        public FormContact(Contact contact)
        {
            InitializeComponent();

            ContactInfo = contact;

            textBoxPrenom.Text = ContactInfo.Prenom;
            textBoxNom.Text = ContactInfo.Nom;
            textBoxCourriel.Text = ContactInfo.Courriel;
            textBoxEntreprise.Text = ContactInfo.Societe;
            textBoxLien.Text = ContactInfo.Lien;

            labelVarDateCreation.Text = ContactInfo.DateCreation;
            labelVarDateModif.Text = ContactInfo.DateModification;
        }

        private void labelPrenom_Click(object sender, EventArgs e)
        {

        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonValider_Click(object sender, EventArgs e)
        {
            ContactInfo.Prenom = textBoxPrenom.Text;
            ContactInfo.Nom = textBoxNom.Text;
            ContactInfo.Courriel = textBoxCourriel.Text;
            ContactInfo.Societe = textBoxEntreprise.Text;
            ContactInfo.Lien = textBoxLien.Text;
            ContactInfo.DateModification = Fichier.GetDateActuelle();

            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelVarDateModif_Click(object sender, EventArgs e)
        {

        }
    }
}
