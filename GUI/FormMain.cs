
using Serializer;
using ContactManager;
using System.Windows.Forms;

namespace GUI
{
    public partial class FormMain : Form
    {
        SerializerService serializerService = new();
        Dossier root = new() { Nom = "root" };

        TreeNode NoeudSelectionne;

        public FormMain()
        {
            InitializeComponent();
            treeViewFiles.Nodes.Add(root.ToString());
            NoeudSelectionne = treeViewFiles.Nodes[0];
            UpdateArborescence();

            treeViewFiles.NodeMouseClick += ClickGaucheTreeView;
            

            //Configuration de contextMenuStripOptionsFichier
            contextMenuStripOptionsFichier = new ContextMenuStrip();
            contextMenuStripOptionsFichier.Items.AddRange(new ToolStripMenuItem[] {
                new ("Propi�t�s...", null, (s, e) => MettreAJourFichier(NoeudSelectionne)),
                new ("Supprimer", null, (s, e) => SupprimerFichier(NoeudSelectionne)),
                new ("Cr�er un dossier...", null, (s, e) => CreerFichier(NoeudSelectionne, typeof(Dossier))),
                new ("Cr�er un contact...", null, (s, e) => CreerFichier(NoeudSelectionne, typeof(Contact)))
            });

        }

        /* Permet de cr�er un nouveau fichier dans l'arborescence */
        private void CreerFichier(TreeNode? treeNode, Type typeACreer)
        {
            if (treeNode != null)
            {
                Dossier dossier;
                Fichier fichier = (Fichier)treeNode.Tag;

                // On r�cup�re le dossier dans lequel on doit cr�er le fichier
                if (fichier.EstUnDossier())
                {
                    dossier = (Dossier)fichier;
                }
                else
                {
                    // Condition toujours vraie dans le contexte mais bon...
                    if (fichier.Parent != null)
                    {   
                        dossier = fichier.Parent;
                    }
                    else
                    {
                        dossier = root;
                    }
                }

                // On cr�er le nouveau fichier
                if (typeACreer == typeof(Dossier))
                {
                    Dossier nouveauDossier = new();
                    dossier.ajouterFichier(nouveauDossier);
                    Prompt.ShowDossierUpdaterDialog(nouveauDossier);
                }
                else
                {
                    Contact nouveauContact = new();
                    dossier.ajouterFichier(nouveauContact);
                    Prompt.ShowContactUpdaterDialog(nouveauContact);
                }

                UpdateArborescence();
            }
        }
        private void SupprimerFichier(TreeNode? treeNode)
        {
            if (treeNode != null)
            {
                Fichier fichier = (Fichier)treeNode.Tag;
                fichier.Supprimer();
                UpdateArborescence();
            }
        }
        private void ClickGaucheTreeView(object? sender, TreeNodeMouseClickEventArgs e)
        {   
            
            if (e.Button == MouseButtons.Right)
            {
                NoeudSelectionne = e.Node;
                //MessageBox.Show("Clique gauche sur l'arbre !!!");
                contextMenuStripOptionsFichier.Show(Cursor.Position);
            }
        }
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            string mdp = Prompt.ShowDialog("Entrez votre mot de passe : ", "Mot de passe");
            if (mdp == "") mdp = "0000";
            try
            {
                root = serializerService.Deserialize(mdp);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Mauvais mot de passe.");
            }

            UpdateArborescence();

        }
        private void MettreAJourFichier(TreeNode? treeNode)
        {   
            if (treeNode != null)
            {
                if (treeNode.Tag.GetType() == typeof(Dossier))
                {
                    Dossier dossier = (Dossier)treeNode.Tag;
                    Prompt.ShowDossierUpdaterDialog(dossier);

                }
                else if (treeNode.Tag.GetType() == typeof(Contact))
                {
                    Contact contact = (Contact)treeNode.Tag;
                    Prompt.ShowContactUpdaterDialog(contact);
                }

                UpdateArborescence();
            }  
        }

        public void UpdateArborescence()
        {
            treeViewFiles.BeginUpdate();

            treeViewFiles.Nodes[0].Remove();
            treeViewFiles.Nodes.Add(root.Nom);
            treeViewFiles.Nodes[0].Tag = root;
            AfficherArboresenceRec(root, treeViewFiles.Nodes[0].Nodes);
            treeViewFiles.ExpandAll();

            treeViewFiles.EndUpdate();
        }
        void AfficherArboresenceRec(Dossier dossier, TreeNodeCollection noeud)
        {
            int k = 0;
            foreach (Fichier fichier in dossier)
            {
                

                if (fichier.EstUnDossier())
                {
                    noeud.Add(fichier.Nom);
                    noeud[k].Tag = fichier;
                    AfficherArboresenceRec((Dossier)fichier, noeud[k].Nodes);
                }
                else
                {
                    noeud.Add(fichier.ToString());
                    noeud[k].Tag = fichier;
                }
                k++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mdp = Prompt.ShowDialog("Entrez un mot de passe : ", "Mot de passe");
            if (mdp == "") mdp = "0000";

            serializerService.Serialize(root, mdp);
        }

        private void NoeudClickGauche(object sender, EventArgs e)
        {
            MessageBox.Show("Clique gauche sur un noeud !!!");
        }
    }

    /* Classe qui regroupe diff�rentes bo�tes de dialogue
     */
    public static class Prompt
    {
        /* M�thode trouv�e sur :
         * https://stackoverflow.com/questions/5427020/prompt-dialog-in-windows-forms
         */
        public static string ShowDialog(string text, string caption)
        {
            Form prompt = new Form()
            {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }

        public static void ShowContactUpdaterDialog(Contact contact)
        {
            FormContact form = new(contact);

            form.ShowDialog();
        }

        public static void ShowDossierUpdaterDialog(Dossier dossier)
        {
            FormDossier form = new(dossier);

            form.ShowDialog();
        }
    }
}
