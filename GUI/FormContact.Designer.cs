﻿using ContactManager;

namespace GUI
{
    partial class FormContact
    {
        Contact ContactInfo { get; set; }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            labelPrenom = new Label();
            labelNom = new Label();
            labelCourriel = new Label();
            textBoxPrenom = new TextBox();
            textBoxNom = new TextBox();
            textBoxCourriel = new TextBox();
            groupBox1 = new GroupBox();
            textBoxLien = new TextBox();
            labelLien = new Label();
            textBoxEntreprise = new TextBox();
            labelEntreprise = new Label();
            panel1 = new Panel();
            buttonAnnuler = new Button();
            buttonValider = new Button();
            labelDateCreation = new Label();
            panel2 = new Panel();
            labelVarDateCreation = new Label();
            labelDateModification = new Label();
            labelVarDateModif = new Label();
            groupBox1.SuspendLayout();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // labelPrenom
            // 
            labelPrenom.AutoSize = true;
            labelPrenom.Location = new Point(6, 35);
            labelPrenom.Name = "labelPrenom";
            labelPrenom.Size = new Size(49, 15);
            labelPrenom.TabIndex = 0;
            labelPrenom.Text = "Prénom";
            labelPrenom.Click += labelPrenom_Click;
            // 
            // labelNom
            // 
            labelNom.AutoSize = true;
            labelNom.Location = new Point(6, 88);
            labelNom.Name = "labelNom";
            labelNom.Size = new Size(34, 15);
            labelNom.TabIndex = 1;
            labelNom.Text = "Nom";
            // 
            // labelCourriel
            // 
            labelCourriel.AutoSize = true;
            labelCourriel.Location = new Point(6, 144);
            labelCourriel.Name = "labelCourriel";
            labelCourriel.Size = new Size(49, 15);
            labelCourriel.TabIndex = 2;
            labelCourriel.Text = "Courriel";
            // 
            // textBoxPrenom
            // 
            textBoxPrenom.Location = new Point(6, 53);
            textBoxPrenom.Name = "textBoxPrenom";
            textBoxPrenom.Size = new Size(179, 23);
            textBoxPrenom.TabIndex = 3;
            // 
            // textBoxNom
            // 
            textBoxNom.Location = new Point(6, 106);
            textBoxNom.Name = "textBoxNom";
            textBoxNom.Size = new Size(179, 23);
            textBoxNom.TabIndex = 4;
            // 
            // textBoxCourriel
            // 
            textBoxCourriel.Location = new Point(6, 162);
            textBoxCourriel.Name = "textBoxCourriel";
            textBoxCourriel.Size = new Size(179, 23);
            textBoxCourriel.TabIndex = 5;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(textBoxLien);
            groupBox1.Controls.Add(labelLien);
            groupBox1.Controls.Add(textBoxEntreprise);
            groupBox1.Controls.Add(labelEntreprise);
            groupBox1.Controls.Add(textBoxPrenom);
            groupBox1.Controls.Add(textBoxCourriel);
            groupBox1.Controls.Add(labelPrenom);
            groupBox1.Controls.Add(textBoxNom);
            groupBox1.Controls.Add(labelNom);
            groupBox1.Controls.Add(labelCourriel);
            groupBox1.Location = new Point(40, 44);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(211, 318);
            groupBox1.TabIndex = 6;
            groupBox1.TabStop = false;
            groupBox1.Text = "Informations";
            // 
            // textBoxLien
            // 
            textBoxLien.Location = new Point(6, 283);
            textBoxLien.Name = "textBoxLien";
            textBoxLien.Size = new Size(179, 23);
            textBoxLien.TabIndex = 9;
            textBoxLien.TextChanged += textBox1_TextChanged;
            // 
            // labelLien
            // 
            labelLien.AutoSize = true;
            labelLien.Location = new Point(6, 265);
            labelLien.Name = "labelLien";
            labelLien.Size = new Size(29, 15);
            labelLien.TabIndex = 8;
            labelLien.Text = "Lien";
            // 
            // textBoxEntreprise
            // 
            textBoxEntreprise.Location = new Point(6, 223);
            textBoxEntreprise.Name = "textBoxEntreprise";
            textBoxEntreprise.Size = new Size(179, 23);
            textBoxEntreprise.TabIndex = 7;
            // 
            // labelEntreprise
            // 
            labelEntreprise.AutoSize = true;
            labelEntreprise.Location = new Point(6, 205);
            labelEntreprise.Name = "labelEntreprise";
            labelEntreprise.Size = new Size(59, 15);
            labelEntreprise.TabIndex = 6;
            labelEntreprise.Text = "Entreprise";
            labelEntreprise.Click += label1_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(buttonAnnuler);
            panel1.Controls.Add(buttonValider);
            panel1.Location = new Point(257, 327);
            panel1.Name = "panel1";
            panel1.Size = new Size(175, 35);
            panel1.TabIndex = 7;
            // 
            // buttonAnnuler
            // 
            buttonAnnuler.Location = new Point(84, 3);
            buttonAnnuler.Name = "buttonAnnuler";
            buttonAnnuler.Size = new Size(75, 23);
            buttonAnnuler.TabIndex = 1;
            buttonAnnuler.Text = "Annuler";
            buttonAnnuler.UseVisualStyleBackColor = true;
            buttonAnnuler.Click += buttonAnnuler_Click;
            // 
            // buttonValider
            // 
            buttonValider.Location = new Point(3, 3);
            buttonValider.Name = "buttonValider";
            buttonValider.Size = new Size(75, 23);
            buttonValider.TabIndex = 0;
            buttonValider.Text = "Valider";
            buttonValider.UseVisualStyleBackColor = true;
            buttonValider.Click += buttonValider_Click;
            // 
            // labelDateCreation
            // 
            labelDateCreation.AutoSize = true;
            labelDateCreation.Location = new Point(3, 10);
            labelDateCreation.Name = "labelDateCreation";
            labelDateCreation.Size = new Size(99, 15);
            labelDateCreation.TabIndex = 8;
            labelDateCreation.Text = "Date de création :";
            // 
            // panel2
            // 
            panel2.Controls.Add(labelVarDateModif);
            panel2.Controls.Add(labelDateModification);
            panel2.Controls.Add(labelVarDateCreation);
            panel2.Controls.Add(labelDateCreation);
            panel2.Location = new Point(260, 53);
            panel2.Name = "panel2";
            panel2.Size = new Size(223, 129);
            panel2.TabIndex = 9;
            // 
            // labelVarDateCreation
            // 
            labelVarDateCreation.AutoSize = true;
            labelVarDateCreation.Location = new Point(3, 25);
            labelVarDateCreation.Name = "labelVarDateCreation";
            labelVarDateCreation.Size = new Size(38, 15);
            labelVarDateCreation.TabIndex = 9;
            labelVarDateCreation.Text = "[date]";
            // 
            // labelDateModification
            // 
            labelDateModification.AutoSize = true;
            labelDateModification.Location = new Point(3, 59);
            labelDateModification.Name = "labelDateModification";
            labelDateModification.Size = new Size(128, 15);
            labelDateModification.TabIndex = 10;
            labelDateModification.Text = "Dernière modification :";
            // 
            // labelVarDateModif
            // 
            labelVarDateModif.AutoSize = true;
            labelVarDateModif.Location = new Point(3, 74);
            labelVarDateModif.Name = "labelVarDateModif";
            labelVarDateModif.Size = new Size(38, 15);
            labelVarDateModif.TabIndex = 11;
            labelVarDateModif.Text = "[date]";
            labelVarDateModif.Click += labelVarDateModif_Click;
            // 
            // FormContact
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(587, 398);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Controls.Add(groupBox1);
            Name = "FormContact";
            Text = "FormContact";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            panel1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Label labelPrenom;
        private Label labelNom;
        private Label labelCourriel;
        private TextBox textBoxPrenom;
        private TextBox textBoxNom;
        private TextBox textBoxCourriel;
        private GroupBox groupBox1;
        private Panel panel1;
        private Button buttonAnnuler;
        private Button buttonValider;
        private Label labelEntreprise;
        private TextBox textBoxLien;
        private Label labelLien;
        private TextBox textBoxEntreprise;
        private Label labelDateCreation;
        private Panel panel2;
        private Label labelVarDateModif;
        private Label labelDateModification;
        private Label labelVarDateCreation;
    }
}