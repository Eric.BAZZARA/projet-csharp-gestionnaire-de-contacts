﻿namespace GUI
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            buttonLoad = new Button();
            treeViewFiles = new TreeView();
            buttonSave = new Button();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(buttonSave);
            groupBox1.Controls.Add(buttonLoad);
            groupBox1.Location = new Point(12, 338);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(436, 100);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "groupBox1";
            // 
            // buttonLoad
            // 
            buttonLoad.Location = new Point(6, 22);
            buttonLoad.Name = "buttonLoad";
            buttonLoad.Size = new Size(75, 23);
            buttonLoad.TabIndex = 0;
            buttonLoad.Text = "Charger";
            buttonLoad.UseVisualStyleBackColor = true;
            buttonLoad.Click += buttonLoad_Click;
            // 
            // treeViewFiles
            // 
            treeViewFiles.Location = new Point(12, 12);
            treeViewFiles.Name = "treeViewFiles";
            treeViewFiles.Size = new Size(776, 320);
            treeViewFiles.TabIndex = 1;
            // 
            // buttonSave
            // 
            buttonSave.Location = new Point(87, 22);
            buttonSave.Name = "buttonSave";
            buttonSave.Size = new Size(95, 23);
            buttonSave.TabIndex = 1;
            buttonSave.Text = "Sauvegarder";
            buttonSave.UseVisualStyleBackColor = true;
            buttonSave.Click += button1_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(treeViewFiles);
            Controls.Add(groupBox1);
            Name = "Form1";
            Text = "Form1";
            groupBox1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private Button buttonLoad;
        private TreeView treeViewFiles;
        private Button buttonSave;
        private ContextMenuStrip contextMenuStripOptionsFichier;
    }
}
